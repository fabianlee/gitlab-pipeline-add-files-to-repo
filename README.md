# Example showing how to add file to repo during pipeline run

On push or merge requests, runs GitLab CI/CD Pipeline that adds file to git repository as part of processing.

The file is added as "reports/file-&lt;timestamp&gt;.txt"


## Pushing to GitLab repo from pipeline

The CI_JOB_TOKEN does not have adequate permissions to write to the repository, so instead we rely on a project level Access Token.

Project Settings > Access Tokens > 'for-ci-cd' with roles: read_repository,write_repository

This value is then set in Project Settings > CI/CD > Variables as the variable 'PROJECT_ACCESS_TOKEN'.


## Testing Merge Request

```
# create new temporary branch for MR changes
new_branch=$(whoami)-main-$RANDOM ; git checkout -b $new_branch

# make simple modification
echo -en "\n$new_branch" >> README.md

# push to new temporary branch, will output URL where MR can be created
git commit -a -m "new MR on branch $new_branch"; git push -u origin $new_branch

# after MR fully merged, remove temp branch
git checkout main && git pull -r
git branch -d $new_branch
```

